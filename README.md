Config sample of `disks` section from `config/filesystems.php` file:

```php
'ftpd' => [
    'driver' => 'ftpd',
    'host' => env('FTP_HOST', '127.0.0.1'),
    'username' => env('FTP_USERNAME', 'laravel'),
    'password' => env('FTP_PASSWORD', 'laravel'),
    'port' => env('FTP_PORT', 21),
]```
