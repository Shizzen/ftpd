<?php

namespace Shizzen\Ftpd;

use League\Flysystem\{
    Filesystem,
    Adapter\Ftpd as FtpdAdapter
};
use Illuminate\Support\ServiceProvider;

class FtpdServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->make('filesystem')->extend('ftpd', function ($app, $config) {
            return new Filesystem(
                new FtpdAdapter($config), $config
            );
        });
    }
}
